#!/usr/bin/env python3

# imports
import os
import time

# $ go get github.com/Sirupsen/logrus
# $ go get github.com/timest/gomanuf
# $ go get github.com/google/gopacket

Y = (['yes', 'y', 'YES', 'Y'])
N = (['no', 'n', 'NO', 'N'])

install = input("Want to install networkscanner Y / N: ")

def installation():
    if install in Y:
        print('checking go version')
        time.sleep(1)
        os.system('go version')
        time.sleep(1)
        print('installing go deps')
        time.sleep(1)
        os.system('go get github.com/Sirupsen/logrus')
        os.system('go get github.com/timest/gomanuf')
        os.system('go get github.com/google/gopacket')

    elif install in N:
        print('OK BYE')

def main():
    installation()


if __name__ == "__main__":
    print('NETWORK SCANNER INSTALLATION MENU')
    time.sleep(1)
    main()
