package main

import (
    "net"
    "github.com/Sirupsen/logrus"
    "time"
    "fmt"
    "sync"
    "sort"
    "context"
    "os"
    "strings"
    manuf "github.com/timest/gomanuf"
    "flag"
)

var log = logrus.New()

var ipNet *net.IPNet

var localHaddr net.HardwareAddr
var iface string

var data map[string]Info

var t *time.Ticker
var do chan string

const (
    START = "start"
    END = "end"
)

type Info struct {
    Mac      net.HardwareAddr
    Hostname string
    Manuf    string
}

func PrintData() {
    var keys IPSlice
    for k := range data {
        keys = append(keys, ParseIPString(k))
    }
    sort.Sort(keys)
    for _, k := range keys {
        d := data[k.String()]
        mac := ""
        if d.Mac != nil {
            mac = d.Mac.String()
        }
        fmt.Printf("%-15s %-17s %-30s %-10s\n", k.String(), mac, d.Hostname, d.Manuf)
    }
}

func pushData(ip string, mac net.HardwareAddr, hostname, manuf string) {
    do <- START
    var mu sync.RWMutex
    mu.RLock()
    defer func() {
        do <- END
        mu.RUnlock()
    }()
    if _, ok := data[ip]; !ok {
        data[ip] = Info{Mac: mac, Hostname: hostname, Manuf: manuf}
        return
    }
    info := data[ip]
    if len(hostname) > 0 && len(info.Hostname) == 0 {
        info.Hostname = hostname
    }
    if len(manuf) > 0 && len(info.Manuf) == 0 {
        info.Manuf = manuf
    }
    if mac != nil {
        info.Mac = mac
    }
    data[ip] = info
}

func setupNetInfo(f string) {
    var ifs []net.Interface
    var err error
    if f == "" {
        ifs, err = net.Interfaces()
    } else {
        var it *net.Interface
        it, err = net.InterfaceByName(f)
        if err == nil {
            ifs = append(ifs, *it)
        }
    }
    if err != nil {
        log.Fatal("some error:", err)
    }
    for _, it := range ifs {
        addr, _ := it.Addrs()
        for _, a := range addr {
            if ip, ok := a.(*net.IPNet); ok && !ip.IP.IsLoopback() {
                if ip.IP.To4() != nil {
                    ipNet = ip
                    localHaddr = it.HardwareAddr
                    iface = it.Name
                    goto END
                }
            }
        }
    }
    END:
    if ipNet == nil || len(localHaddr) == 0 {
        log.Fatal("some error")
    }
}

func localHost() {
    host, _ := os.Hostname()
    data[ipNet.IP.String()] = Info{Mac: localHaddr, Hostname: strings.TrimSuffix(host, ".local"), Manuf: manuf.Search(localHaddr.String())}
}

func sendARP() {
    ips := Table(ipNet)
    for _, ip := range ips {
        go sendArpPackage(ip)
    }
}

func main() {
    if os.Geteuid() != 0 {
        log.Fatal("goscan must run as root.")
    }
    flag.StringVar(&iface, "I", "", "Network interface name")
    flag.Parse()
    data = make(map[string]Info)
    do = make(chan string)
    setupNetInfo(iface)
    
    ctx, cancel := context.WithCancel(context.Background())
    go listenARP(ctx)
    go listenMDNS(ctx)
    go listenNBNS(ctx)
    go sendARP()
    go localHost()
    
    t = time.NewTicker(4 * time.Second)
    for {
        select {
        case <-t.C:
            PrintData()
            cancel()
            goto END
        case d := <-do:
            switch d {
            case START:
                t.Stop()
            case END:

                t = time.NewTicker(2 * time.Second)
            }
        }
    }
    END:
    
}
